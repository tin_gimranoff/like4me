class CreateDownloadOrders < ActiveRecord::Migration
  def change
    create_table :download_orders do |t|
      t.integer :vkid
      t.string :type
      t.integer :count_users
      t.integer :user_id

      t.timestamps
    end
  end
end
