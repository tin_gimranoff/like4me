class CreateTarifs < ActiveRecord::Migration
  def self.up
    create_table :tarifs do |t|
      t.integer :hearts
      t.integer :price
      t.integer :table_num

      t.timestamps
    end
      
      
    execute "insert into tarifs (hearts, price, table_num) values (250, 150, 1)"  
    execute "insert into tarifs (hearts, price, table_num) values (500, 200, 1)" 
    execute "insert into tarifs (hearts, price, table_num) values (1400, 500, 1)" 
    execute "insert into tarifs (hearts, price, table_num) values (3000, 1000, 1)" 
    execute "insert into tarifs (hearts, price, table_num) values (10000, 3000, 1)"     

    execute "insert into tarifs (hearts, price, table_num) values (21000, 6000, 2)" 
    execute "insert into tarifs (hearts, price, table_num) values (70000, 18000, 2)" 
    execute "insert into tarifs (hearts, price, table_num) values (160000, 36000, 2)" 
    execute "insert into tarifs (hearts, price, table_num) values (340000, 66300, 2)"  
    end
end
