class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.string :payment_num
      t.integer :user_id
      t.integer :amount
      t.integer :status, :default => 0
      t.timestamps
    end
  end
end
