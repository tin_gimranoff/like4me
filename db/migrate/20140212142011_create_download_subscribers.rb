class CreateDownloadSubscribers < ActiveRecord::Migration
  def change
    create_table :download_subscribers do |t|
      t.string :url
      t.string :type
      t.integer :user_id

      t.timestamps
    end
  end
end
