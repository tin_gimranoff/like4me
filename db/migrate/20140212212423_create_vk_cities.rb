class CreateVkCities < ActiveRecord::Migration
  
  include AdminHelper
  
  def change
    create_table :vk_cities do |t|
      t.integer :vkid
      t.string :city
      t.integer :city_id
      t.integer :city_vkid

      t.timestamps
    end
    
    access_token = get_settings_value('access_token')
    vk = VkontakteApi::Client.new(access_token)
    countries = VkCountries.all
    countries.each do |c|
      cities = vk.places.getCities(country: c.vkid)
      cities.each do |city|
        execute 'insert into vk_cities (vkid, city, city_id, city_vkid) values ('+city.cid.to_s+',"'+city.title.to_s+'", '+c.id.to_s+', '+c.vkid.to_s+')'
      end 
      sleep 3   
    end
  end
end
