class CreateVkCountries < ActiveRecord::Migration
  
  include AdminHelper
  
  def change
    create_table :vk_countries do |t|
      t.integer :vkid
      t.string :country

      t.timestamps
    end
        
    access_token = get_settings_value('access_token')
    vk = VkontakteApi::Client.new(access_token)
    countries = vk.places.getCountries(need_full: 1)    
    countries.each do |c|
       execute "insert into vk_countries (vkid, country) values ("+c.cid.to_s+",'"+c.title.to_s+"')"
    end
    
    sleep 3
    
    countries = vk.places.getCountries
    countries.each do |c|
       if VkCountries.count(:conditions => {:vkid => c.cid.to_i}) == 0
        execute "insert into vk_countries (vkid, country) values ("+c.cid.to_s+",'"+c.title.to_s+"')"
       end
    end
    
  end
end
