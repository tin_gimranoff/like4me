class CreateFriendOrders < ActiveRecord::Migration
  def change
    create_table :friend_orders do |t|
      t.string :name
      t.string :image
      t.string :url
      t.integer :balance
      t.integer :bonus
      t.integer :sex
      t.integer :uid
      t.integer :gender_min
      t.integer :gender_max
      t.integer :user_id
      t.integer :time_expiration
      
      t.timestamps
    end
  end
end