class AddInfoToSiteUser < ActiveRecord::Migration
  def change
    add_column :site_users, :info, :integer
  end
end
