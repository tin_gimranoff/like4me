class CreateExchanges < ActiveRecord::Migration
  def change
    create_table :exchanges do |t|
      t.string :wmr
      t.integer :balance
      t.integer :user_id
      t.integer :status, :default => 0
      t.timestamps
    end
  end
end
