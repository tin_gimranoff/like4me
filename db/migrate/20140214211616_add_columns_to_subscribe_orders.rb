class AddColumnsToSubscribeOrders < ActiveRecord::Migration
  def change
    add_column :subscribe_orders, :sex, :integer
    add_column :subscribe_orders, :gender_min, :integer
    add_column :subscribe_orders, :gender_max, :integer
    add_column :subscribe_orders, :country, :integer
  end
end
