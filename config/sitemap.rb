Sitemap::Generator.instance.load :host => "like4me.ru" do

  path :root, :priority => 1
  literal "/buy"
  literal "/howto"
  literal "/services"
  literal "/faq"
  literal "/feedback"
  path :blog
  Post.find(:all, :conditions => {:status => 0}).each do |post|
    literal "/blog/"+post.id.to_s
  end
end
