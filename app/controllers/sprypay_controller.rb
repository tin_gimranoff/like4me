# coding: utf-8
require 'digest/md5'

class SprypayController < ApplicationController
  def success
    redirect_to '/cabinet?success=1'
  end

  def fail
    redirect_to '/cabinet?fail=1'
  end
  
  def ipn
    #Получаем параметры
    spAmount = params[:spAmount]
    spShopPaymentId = params[:spShopPaymentId]
    spHashString = params[:spHashString].upcase
    spUserDataUserId = params[:spUserDataUserId]
    spPaymentId = params[:spPaymentId]
    spShopId = params[:spShopId]
    spBalanceAmount = params[:spBalanceAmount]
    spCurrency = params[:spCurrency]
    spCustomerEmail = params[:spCustomerEmail]
    spPurpose = params[:spPurpose]
    spPaymentSystemId = params[:spPaymentSystemId]
    spPaymentSystemAmount = params[:spPaymentSystemAmount]
    spPaymentSystemPaymentId = params[:spPaymentSystemPaymentId]
    spEnrollDateTime = params[:spEnrollDateTime]
    secret_key = 'd6675ec42b25333f937b7c81c71de73d'
    
    @payment = Payment.count(:conditions => {:payment_num => spShopPaymentId});
    if @payment != 0
      render :text => 'error: Payment is exist'
      return
    end
       
    my_sig_val = Digest::MD5.hexdigest(spPaymentId+spShopId+spShopPaymentId+spBalanceAmount+spAmount+spCurrency+spCustomerEmail+spPurpose+spPaymentSystemId+spPaymentSystemAmount+spPaymentSystemPaymentId+spEnrollDateTime+secret_key).upcase 
    if my_sig_val != spHashString
       render :text => 'error: FAIL'
       return
    end
    @payment = Payment.new
    @payment[:user_id] = spUserDataUserId
    @payment[:amount] = spAmount.to_i
    @payment[:payment_num] = spShopPaymentId
    @payment[:status] = 1
    site_user = SiteUser.find(:first, :conditions => {:id => spUserDataUserId })
    status_update = 0
    @tarif = Tarif.find(:first, :conditions => { :price => spAmount.to_i })
    if @tarif 
      site_user.update_attributes(:balance => site_user.balance.to_i + @tarif.hearts.to_i)
      status_update = 1
    else
      site_user.update_attributes(:balance => site_user.balance.to_i + spAmount.to_i)
      status_update = 1
    end
    
    if status_update == 1
      if @payment.save
        render :text => 'ok'
        return
      end 
    else
      render :text => 'error: FAIL'  
      return  
    end
  end
end