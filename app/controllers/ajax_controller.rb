# coding: utf-8
class AjaxController < ApplicationController
  
  include AdminHelper  
  
  def do_not_show_info_win
    if current_user
      SiteUser.find(current_user.id).update_attributes(:info => 1)
      render :text => 1
    end
  end
  
  def check_info_win
    if current_user
      if SiteUser.find(current_user.id).info.to_i == 1
        render :text => 1
      else
        render :text => 0
      end  
    end
  end
  
  #Выбираем пост который надо лайкнуть
  def like_post
    #Выбираем все посты подходящие под не нудевой баланс и не создавал текущий пользователь
    orders = LikeOrder.find(:all, 
        :conditions => ["balance > ? AND user_id <> ?", 0, current_user.id],
        :order => "RAND()"
    )
    #Идем по всем постам
    orders.each do |item|
      #Если не указан пол или пол совпадает с текщим пользователем
      if item.sex.to_i == 0 || item.sex.to_i == current_user.sex.to_i
        #Если минимальный возраст не указан или дата рождениия пользователя не скрыта и больше указанной в зказе
        if item.gender_min.to_i == 0 || (current_user.birthday != nil && (((Date.today-Date.parse(current_user.birthday.to_s)).to_i)/365).to_i >= item.gender_min.to_i) 
          #Аналогично с максимальным возрастом
          if item.gender_max.to_i == 0 || (current_user.birthday != nil && (((Date.today-Date.parse(current_user.birthday.to_s)).to_i)/365).to_i <= item.gender_max.to_i)
            #Создаем объект для работы с ВК 
            vk = VkontakteApi::Client.new(get_settings_value('access_token'))
            #Проверям не лайкал ли текущий пользователь данный пост
            liked = vk.likes.is_liked(owner_id: item.owner_id, item_id: item.item_id, type: item.type_record, user_id: current_user.uid)
            if liked.to_i == 0
              #Если все условия соблюдены то выбираем данный УРЛ
              render :text => item.url
              return
            end
          end 
        end
      end
    end
    render :text => 0
    return
  end
  
  #Проверяем лайкнул ли пользователь пост
  def check_like
    #Если нам пришел УРЛ
    if params[:url]
      #Выбираем заказ с данным УРЛ
      item = LikeOrder.find(:first, :conditions => { :url => params[:url] })
      #ЕСли заказ найдет и результат не равен nil
      if item && item != nil
        #Если баланс данного заказа уже не равен нулю
        if item.balance.to_i != 0
          #Создаем объект для работы с ВК
          vk = VkontakteApi::Client.new(get_settings_value('access_token'))
          #Проверяем лайкнул ли пользователь данный пост
          liked = vk.likes.is_liked(owner_id: item.owner_id, item_id: item.item_id, type: item.type_record, user_id: current_user.uid)
          #Если лайкнул
          if liked.to_i == 1
            #Прибаляем сердечки к балансу пользоватаелю.
            item.update_attributes(:balance => item.balance.to_i - 1)
            site_user = SiteUser.find(:first, :conditions => {:id => current_user.id })
            site_user.update_attributes(:balance => current_user.balance.to_i + 1)
            render :text => 0
            return
          else
            render :text => 4 #Вы не лайкнули пост
            return
          end
        else
          render :text => 3 #Баланс уже равен нулю
          return; 
        end
      else
        render :text => 2 #Заказа не найдено
        return  
      end

    else
      render :text => 1 #Пришёл пустой параметр
      return
    end
  end
  
  #Подписаться на группу
  def subscribe_group
    #Выбираем все заказы у которых не нулевой баланс и не создан текущим пользователем
    orders = SubscribeOrders.find(:all, 
        :conditions => ["balance > ? AND user_id <> ?", 0, current_user.id],
        :order => "RAND()"
    )
    #Идем по всем заказам
    orders.each do |item|
      if item.sex.to_i == 0 || item.sex.to_i == current_user.sex.to_i
        #Если минимальный возраст не указан или дата рождениия пользователя не скрыта и больше указанной в зказе
        if item.gender_min.to_i == 0 || (current_user.birthday != nil && (((Date.today-Date.parse(current_user.birthday.to_s)).to_i)/365).to_i >= item.gender_min.to_i) 
          #Аналогично с максимальным возрастом
          if item.gender_max.to_i == 0 || (current_user.birthday != nil && (((Date.today-Date.parse(current_user.birthday.to_s)).to_i)/365).to_i <= item.gender_max.to_i)
              #Создаем объект для работы с ВК
              vk = VkontakteApi::Client.new(get_settings_value('access_token'))
              #Проверям является пользователь подписчиком
              is_member = vk.groups.isMember(gid: item.gid, user_id: current_user.uid)
              #Если нет
              if is_member.to_i == 0
                #Отдаем УРЛ
                render :text => item.url
                return
              end
          end
        end
      end
    end
    render :text => 0
    return
  end
  
  #Подписался ли пользователь на группу
  def check_subscribe
    #ЕСли пришел УРЛ
    if params[:url]
      #Выбираем заказ с таким УРЛ
      item = SubscribeOrders.find(:first, :conditions => { :url => params[:url] })
      #Если азказ найдет
      if item && item != nil
        #Проверяем не равен ли баланс уже нулю
        if item.balance.to_i != 0
          #Создаем объект для работы с ВК
          vk = VkontakteApi::Client.new(get_settings_value('access_token'))
          #Является ли пользователем подписчиком
          is_member = vk.groups.isMember(gid: item.gid, user_id: current_user.uid)
          #Если да
          if is_member.to_i == 1
            #Ищем пользователя и увеличиваем баланс
            item.update_attribute('balance', item.balance.to_i - 1)
            site_user = SiteUser.find(:first, :conditions => {:id => current_user.id })
            site_user.update_attributes(:balance => current_user.balance.to_i + item.bonus.to_i)
            render :json => {error_code: 0, bonus: item.bonus}
            return
          else
            render :json => {error_code: 4} #Вы не лайкнули пост
            return
          end
        else
          render :json => {error_code: 3} #Баланс уже равен нулю
          return; 
        end
      else
        render :json => {error_code: 2} #Заказа не найдено
        return  
      end

    else
      render :json => {error_code: 1} #Пришёл пустой параметр
      return
    end
  end
  
  #Обновление баланса
  def update_balance
    #Если тип заказа не лайк, не подписчик и не друг возвращаем ошибку
    if params[:type] != 'like' && params[:type] != 'subscribe' && params[:type] != 'friend'
          render :json => '{ "error":"5" }'
          return
    end
    
    #Если не пришел параметр ID заказа возвращаем ошибку
    if params[:id] == '' || params[:id].to_i.to_s != params[:id]
        render :json => '{ "error":"5" }'
        return
    end
    
    #Если не указан баланс возвращаем ошибку
    if params[:balance] == '' 
        render :json => '{ "error":"4" }'
        return
    end
    
    #Если баланс не целое число возвращаем ошибку
    if params[:balance].to_i.to_s != params[:balance]
        render :json => '{ "error":"3" }'
        return
    end
   
   #Если параметр like
   if params['type'] == 'like'
        #Ищем заказ
        order = LikeOrder.find(:first, :conditions => { :id => params[:id].to_i })
        #Если заказ не найден отсылаем ошибку
        if !order
          render :json => '{ "error":"2" }'
          return 
        end
        #Новый баланс
        new_balance = params[:balance].to_i
        #Если указан пол в заказе
        if order.sex.to_i != 0
          #Увеличиваем новый баланс
          new_balance = new_balance.to_i + params[:balance].to_i
        end 
        #Если указан возврат от или до
        if order.gender_min.to_i != 0 || order.gender_max.to_i != 0
          #Увеличиваем новый баланс
          new_balance = new_balance.to_i + params[:balance].to_i
        end
        #Если новый баланс с учетом параметров больше чем текущий баланс
        if new_balance.to_i > current_user.balance.to_i
          render :json => '{ "error":"1" }' #Шлем ошибку
          return
        end
        #Обновляем баланс заказа
        order.update_attributes(:balance => order.balance.to_i + params[:balance].to_i)
        #Обновляем баланс пользователя
        site_user = SiteUser.find(:first, :conditions => {:id => current_user.id })
        site_user.update_attributes(:balance => current_user.balance.to_i - new_balance.to_i)
        render :json => '{ "error":"0" }'
        return
   end 
   
   #Если тип подписчик
   if params[:type] == 'subscribe'
       #Ищем заказ
       order = SubscribeOrders.find(:first, :conditions => { :id => params[:id].to_i })
       #Если заказ не найден шлем ошибку
       if !order
         render :json => '{ "error":"2" }'
         return 
      end
      #Новый баланс умножаем на бонус
      new_balance = params[:balance].to_i * order.bonus.to_i
      #Если был указан пол увеличиваем баланс
      if order.sex.to_i != 0
        new_balance = new_balance.to_i + params[:balance].to_i
      end 
      #Если был указан возраст от или возраст до увеличиваем баланс
      if order.gender_min.to_i != 0 || order.gender_max.to_i != 0
        new_balance = new_balance.to_i + params[:balance].to_i
      end
      #Если новый баланс больше чем текщий баланс пользователя
      if new_balance.to_i > current_user.balance.to_i
        render :json => '{ "error":"1" }' #Шлем ошибку
        return
      end
      #Обновляем баланс заказа
      order.update_attributes(:balance => order.balance.to_i + params[:balance].to_i)
      #Обновляем баланс пользователя
      site_user = SiteUser.find(:first, :conditions => {:id => current_user.id })
      site_user.update_attributes(:balance => current_user.balance.to_i - new_balance.to_i)
      render :json => '{ "error":"0" }'
      return
   end
   
   #Если пришел параметр "друг"
   if params[:type] == 'friend'
        #Ищем заказ 
        order = FriendOrder.find(:first, :conditions => { :id => params[:id].to_i })
        #Если не нашли шлем ошибку
        if !order
          render :json => '{ "error":"2" }'
          return 
        end
        #Берем новый баланс умножаем на параметр
        new_balance = params[:balance].to_i*order.bonus.to_i
        #Если был указан пол увеличиваем баланс
        if order.sex.to_i != 0
          new_balance = new_balance.to_i + params[:balance].to_i
        end 
        #Если был указан возраст от или возраст до увеличиваем баланс
        if order.gender_min.to_i != 0 || order.gender_max.to_i != 0
          new_balance = new_balance.to_i + params[:balance].to_i
        end
        #Если баланс больше чем текущий баланс поьзователя
        if new_balance.to_i > current_user.balance.to_i
          render :json => '{ "error":"1" }' #шлем ошибку
          return
        end
        #Если время когда заказ истекает больше чем сейчас
        if order.time_expiration > Time.now.to_i
          #Прибавляем к тому времени новый баланс
          order.time_expiration = order.time_expiration + params[:balance].to_i*60
        else
          #Если не больше, то отсчитываем от сейчас
          order.time_expiration = Time.now.to_i + params[:balance].to_i*60
        end
        #Обновляем баланс заказа
        order.update_attributes(:balance => order.get_expiration_time + params[:balance].to_i)
        #Обновляем баланс пользователя
        site_user = SiteUser.find(:first, :conditions => {:id => current_user.id })
        site_user.update_attributes(:balance => current_user.balance.to_i - new_balance.to_i)
        render :json => '{ "error":"0" }'
        return     
   end
    
  end
 
  def service_repost 
      vk = VkontakteApi::Client.new(get_settings_value('access_token'))
      repost = vk.wall.get(owner_id: current_user.uid, count: 1, filter: 'owner')
      if !repost[1].attachment.link.nil? && repost[1].attachment.link.url == 'http://like4me.ru'
        site_user = SiteUser.find(:first, :conditions => {:id => current_user.id })
        site_user.update_attributes(:balance => current_user.balance.to_i + 15.to_i, :repost => 1) 
        render :json => '{ "error":"0" }' 
        return
      else
        render :json => '{ "error":"1" }'
      end
  end
end