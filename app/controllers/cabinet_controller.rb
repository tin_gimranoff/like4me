# coding: utf-8
require 'rest-client'

class CabinetController < ApplicationController

  before_filter :exchange_form

	include AdminHelper  
	
	def exchange_form
	  if !current_user
	    @exchange_show_form = -1
	  end
	  @exchange_show_form = 0
    @exchange = Exchange.new(params[:exchange])
    if request.post? && !params[:exchange].nil?
      @exchange_show_form = 1
      if @exchange.valid?
        if @exchange.balance > current_user.balance
          @exchange.errors.add(:balance, "не может быть больше чем лайков на вашем балансе")      
        else
          @exchange.user_id = current_user.id
          @exchange_show_form = 2 if @exchange.save
        end  
      end
    end
	end

	def index
		if !current_user
			redirect_to '/'
		end
		@meta_title = "Личный кабинет"
    @meta_header = "Заработать сердечки на баланс бесплатно"
    #Забиваем параметры в Хэш
    params = {:user_id => current_user.id, :user_sex => current_user.sex, :uid => current_user.uid.to_i, :time_expiration => Time.now.to_time.to_i}
    #Создаем базовое условие
    condition = "`user_id` <> :user_id AND uid <> :uid AND (`sex` = :user_sex OR `sex` = 0) AND (`time_expiration` >= :time_expiration) AND ((`gender_min` = 0 AND `gender_max` = 0)"
    #Если дата текущего пользователя не равна nil
    if current_user.birthday!=nil 
      #Конвертируем возраст в число
      bdate = (((Date.today-Date.parse(current_user.birthday.to_s)).to_i)/365).to_i
      #Забиваем доп. условия
      condition = condition + " OR (`gender_min` >= "+bdate.to_s+" AND `gender_max` = 0)"
      condition = condition + " OR (`gender_min` = 0 AND `gender_max` <= "+bdate.to_s+")"
      condition = condition + " OR (`gender_min` >= "+bdate.to_s+" AND `gender_max` <= "+bdate.to_s+")"
    end
    condition = condition + ')'
    #Выбираем список заказов
    @friends = FriendOrder.find(:all, :conditions => [condition,  params], :order => 'bonus DESC', :limit => 12)	
	end

  #Форма получить лайки
  def get_likes
      @meta_header = "Накрутить лайки"
      @meta_title = "Накрутить лайки - Личный кабинет"
      #Создаем модель с данными из поста
      @like_order = LikeOrder.new(params[:like_order])
      #Если даынне пришли
      if request.post? && !params[:like_order].nil?
        #Проверяем на валидность
        if @like_order.valid?
          #Пытаемся получить данные от сервера GET запросом
          RestClient.get(params[:like_order][:url]) { |response, request, result, &block|
            case response.code
            #Если поймали 404
            when 404
                #Даем ошибку на недоступность URL
                @like_order.errors.add(:url, "который вы ввели недоступен")
            else
              #Если все ок, получаем страницу
              response = RestClient.get(params[:like_order][:url])
              #Проверяем на наличие внутренней ошибки
              if response.scan(/<title>Ошибка<\/title>/i).size > 0
                #Если ошибка есть то шлем ее пользователю
                @like_order.errors.add(:url, "который вы ввели запрещён для общего доступа настройками приватности")
              else
                #Вычисляем владельца поста
                owner_item_id = params[:like_order][:url].scan(/[-0-9]+_[0-9]+/i)[0].split("_")
                #Начальный баланс считаем столько сколько указал пользователь
                balance = params[:like_order][:balance]
                #Если указан доп параметр "пол" увеличиваем баланс
                if params[:like_order][:sex].to_i != 0
                  balance = balance.to_i + params[:like_order][:balance].to_i
                end

                #Если указан доп. параметр возраст от или до увеличиваем баланс
                if params[:like_order][:gender_min].to_i != 0 || params[:like_order][:gender_max].to_i != 0
                  balance = balance.to_i + params[:like_order][:balance].to_i
                end
                
                #Если получившийся баланс больше чем сейчас у пользователя
                if balance.to_i > current_user.balance.to_i
                  #Шлем ему ошибку
                  @like_order.errors.add(:balance, "не может быть больше чем лайков на вашем балансе")  
                else
                  #Определяем тип записи по URL
                  if params[:like_order][:url].scan(/wall[-0-9]+_[0-9]+/i).size > 0
                    @like_order.type_record = 'post'
                  end

                  if params[:like_order][:url].scan(/photo[-0-9]+_[0-9]+/i).size > 0
                    @like_order.type_record = 'photo'
                  end

                  if params[:like_order][:url].scan(/video[-0-9]+_[0-9]+/i).size > 0
                    @like_order.type_record = 'video'
                  end
                  
                  #Если тип не удалось определить, то шлем пользователю ошибку
                  if @like_order.type_record == nil
                    @like_order.errors.add(:url, "не удаётся установить тип страницы")
                  else
                    #Записываем данные в модель
                    @like_order.owner_id = owner_item_id[0]
                    @like_order.item_id = owner_item_id[1]
                    @like_order.user_id = current_user.id
                    #Если модель удалось сохранить
                    if @like_order.save
                      #Ищем текущего пользователя и списываем его баланс
                      @site_user = SiteUser.find(:first, :conditions => {:id => current_user.id })
                      @site_user.update_attributes(:balance => current_user.balance.to_i - balance.to_i)
                    end
                    #Шлем в предсьавление о том, что все ОК
                    @notice_seccess = 1
                  end
                end
              end
            end
          }
        end
      end
      #Рендерим представление
      render :get_likes
      return       
  end
  
  #Форма получить подписчиков
  def get_subscribers
      @meta_header = "Накрутить подписчиков в группы"
      @meta_title = "Накрутить подписчиков в группы - Личный кабинет"
      #Создаем модель из данных поста
      @subscribe_orders = SubscribeOrders.new(params[:subscribe_orders])
      #Если пришел пост
      if request.post? && !params[:subscribe_orders].nil?
        #Проверяем его валидность
        if @subscribe_orders.valid?
          #Пытаемся получить уникальное имя или gid
          if params[:subscribe_orders][:url].scan(/^http:\/\/vk.com\/club[0-9]+$/i).size > 0 || params[:subscribe_orders][:url].scan(/^http:\/\/vk.com\/public[0-9]+$/i).size > 0
            gid = params[:subscribe_orders][:url].scan(/[0-9]+$/i)
          elsif params[:subscribe_orders][:url].scan(/^http:\/\/vk.com\/[0-9A-Za-z.-_]+$/i).size > 0
            gid = params[:subscribe_orders][:url].split("/")[3]        
          else
            #Если не удается получить шлем ошибку
            @subscribe_orders.errors.add(:url, "не удаётся прочитать информацию о группе")
            #Рендерим представление
            render :get_subscribers
            return
          end
          #Отсылаем ГЕТ запрос на УРЛ
          RestClient.get(params[:subscribe_orders][:url]) { |response, request, result, &block|
            case response.code
            #Если поймали 404
            when 404
                #Шлем ошибку
                @subscribe_orders.errors.add(:url, "который вы ввели недоступен")
            else
              #Создаем объект для работы с Vk
              vk = VkontakteApi::Client.new(get_settings_value('access_token'))
              group_info = vk.groups.getById(gid: gid)
              #Если мы получили закрытую группу
              if group_info[0].is_closed.to_i != 0
                #Шлем ошибку и рендерим представление
                @subscribe_orders.errors.add(:url, "группа закрыта настройками приватности")
                render :get_subscribers
                return
              else
                #Умножаем колечтво подписчиков на бонус и проверяем не больше ли чем балан пользователя
                if params[:subscribe_orders][:balance].to_i*params[:subscribe_orders][:bonus].to_i > current_user.balance.to_i
                  @subscribe_orders.errors.add(:balance, "умноженное на цену подписчика не может быть запрошено больше чем лайков на вашем балансе")
                  render :get_subscribers
                  return 
                else
                  #Умножаем баланс на бонус
                  balance = params[:subscribe_orders][:balance].to_i*params[:subscribe_orders][:bonus].to_i 
                  if params[:subscribe_orders][:sex].to_i != 0
                    balance = balance.to_i + params[:subscribe_orders][:balance].to_i
                  end
                  #Если указан доп параметр возраст от или до еще увеличиваем баланс
                  if params[:subscribe_orders][:gender_min].to_i != 0 || params[:subscribe_orders][:gender_max].to_i != 0
                    balance = balance.to_i + params[:subscribe_orders][:balance].to_i
                  end
                  #Если получившийся баланс больше чем баланс поьзователя
                  if balance.to_i > current_user.balance.to_i
                    #Шлем пользователю ошибку
                    @subscribe_orders.errors.add(:balance, "не может быть больше чем сердечек на вашем балансе")  
                    render :get_subscribers
                    return 
                  end
                end
                #Забиваем недостающие данные в модель
                @subscribe_orders.gid = group_info[0].gid
                @subscribe_orders.user_id = current_user.id
                #Если модель сохранилась
                if @subscribe_orders.save
                  #Ищем текщего пользователя и снимаем с его баланса сердечки
                  @site_user = SiteUser.find(:first, :conditions => {:id => current_user.id })
                  @site_user.update_attributes(:balance => current_user.balance.to_i - balance.to_i)
                end
                #Шлем в представление, что все прошло хорошо
                @notice_seccess = 1
              end
            end
          }
        end
      end
      #Рендерим представление
      render :get_subscribers
      return  
  end
  
  #Форма получить друзей
  def get_friends
      @meta_header = "Накрутить друзей"
      @meta_title = "Накрутить друзей - Личный кабинет"
      #Создаем модель из данных поста
      @friend_orders = FriendOrder.new(params[:friend_order])
      #Если пост пришел
      if request.post? && !params[:friend_order].nil?
        #Проверяем валидность
        if @friend_orders.valid?
          #Шлем гед запрос на УРЛ
          RestClient.get(params[:friend_order][:url]) { |response, request, result, &block|
            case response.code
            #Если поймали 404
            when 404
                #Шлем ошибку пользователю
                @friend_orders.errors.add(:url, "который вы ввели недоступен")
            else
                #Пытаемся получить содержимое страницы
                response = RestClient.get(params[:friend_order][:url])
                #Если получили тайтл с Ошибкой
                if response.scan(/<title>Ошибка<\/title>/i).size > 0
                  #Шлем ошибку пользователю
                  @friend_orders.errors.add(:url, "произошла неизвестная ошибка, проверьте доступность страницы")
                else
                  #Берем в качестве баланса количество минут умноженное на бонус за 1 мин
                  balance = params[:friend_order][:balance].to_i*params[:friend_order][:bonus].to_i
                  #Если указан доп. параметр Пол прибавляем еще количество минут
                  if params[:friend_order][:sex].to_i != 0
                    balance = balance.to_i + params[:friend_order][:balance].to_i
                  end
                  #Если указан доп параметр возраст от или до еще увеличиваем баланс
                  if params[:friend_order][:gender_min].to_i != 0 || params[:friend_order][:gender_max].to_i != 0
                    balance = balance.to_i + params[:friend_order][:balance].to_i
                  end
                  #Если получившийся баланс больше чем баланс поьзователя
                  if balance.to_i > current_user.balance.to_i
                    #Шлем пользователю ошибку
                    @friend_orders.errors.add(:balance, "не может быть больше чем сердечек на вашем балансе")  
                  else
                    #Заполняем данные модели
                    @friend_orders.user_id = current_user.id
                    #Получаем ID пользователя или его уникальное имя
                    gid = params[:friend_order][:url].split("/")[3]
                    #Создаем объект для работы с ВК
                    vk = VkontakteApi::Client.new
                    #Забрашиваем инфо юзер 
                    user_info = vk.users.get(uids: gid, fields: 'photo_medium')
                    #Заоплняем данные модели
                    @friend_orders.uid = user_info[0].uid
                    @friend_orders.name = user_info[0].first_name+' '+user_info[0].last_name
                    @friend_orders.image = user_info[0].photo_medium
                    @friend_orders.calculate_time_expiration
                    #Если модель успешно сохранена
                    if @friend_orders.save
                      #Ищем пользователя и снимаем сердечки с баланса
                      @site_user = SiteUser.find(:first, :conditions => {:id => current_user.id })
                      @site_user.update_attributes(:balance => current_user.balance.to_i - balance.to_i)
                    end
                    #Шлем успешное уведомление в представление
                    @notice_seccess = 1
                  end
                end
            end
          }
        end
      end
      #Рендерим представление
      render :get_friends 
      return    
  end
	
	#Заказы
	def orders
	  if !current_user
 			redirect_to '/'
 		end
 		#Если пришел запрос на удаление
 		if request.delete?
 		  #Ищем заказ на удаление среди лайков
 		  like = LikeOrder.find(:first, :conditions => { :id => params[:param_name] })
 		  #Если нашелся и текущий пользователь создавал заказ и тип заказа на удаление лайк
 		  if like && like.user_id == current_user.id && params[:param_type] == 'like'
 		      #Текущий балан ставим равным остатку баланса заказа
 		      current_balance = like.balance
 		      balance_to_restore = like.balance
 		      #Если был указан пол увеличиваем баланс для восстановления
 		      if like.sex.to_i != 0
 		        balance_to_restore = balance_to_restore.to_i + current_balance.to_i
 		      end
 		      #Если был указан возраст до или от увеличиваем баланс для восстановления
 		      if like.gender_min.to_i != 0 || like.gender_max.to_i != 0
 		         balance_to_restore = balance_to_restore.to_i + current_balance.to_i
 		      end
 		      #Ищем пользователя и возвращаем ему баланс
 		      site_user = SiteUser.find(:first, :conditions => {:id => current_user.id })
					site_user.update_attributes(:balance => current_user.balance.to_i + balance_to_restore.to_i)
					#Удаляем заказ
 		      like.delete
          redirect_to '/cabinet/orders'
 		  end
 		  #Ищем заказ на удаление среди накрутки подписчиков
 		  subscribe = SubscribeOrders.find(:first, :conditions => { :id => params[:param_name] })
 		  #Если нашелся и текущий пользователь создавал заказ и тип заказа на удаление подписчик
 		  if subscribe && subscribe.user_id == current_user.id && params[:param_type] == 'subscribe'
 		      #Берем остаток баланса умножаем на бонус
 		      restore_balance_value = subscribe.balance.to_i*subscribe.bonus.to_i
 		      #Ищем пользователя и возвращаем ему баланс
 		      site_user = SiteUser.find(:first, :conditions => {:id => current_user.id })
					site_user.update_attributes(:balance => current_user.balance.to_i + restore_balance_value.to_i)
					#Удаляем заказ
 		      subscribe.delete
 		      redirect_to '/cabinet/orders'
 		  end
 		  #Ищем заказ среди накрутки друзей
 		  friend = FriendOrder.find(:first, :conditions => { :id => params[:param_name] })
 		  #Если заказ найден создавал его текущий пользователь и его тип равен друг
      if friend && friend.user_id == current_user.id && params[:param_type] == 'friend'
          #Если дата окончания заказа больше чем текущая дата
          if friend.time_expiration.to_i > Time.now.to_i
            #Считаем не использованное время в минутах
            not_used_time = friend.time_expiration.to_i - Time.now.to_i
            restore_balance_value = not_used_time/60
          else
            restore_balance_value = 0;
          end
          #ВОзвращаем пользователю не использованные минуты
          site_user = SiteUser.find(:first, :conditions => {:id => current_user.id })
          site_user.update_attributes(:balance => current_user.balance.to_i + restore_balance_value.to_i)
          #Удаляем заказ
          friend.delete
          redirect_to '/cabinet/orders'
      end
 		end
 		@meta_title = "История заказов - Личный кабинет"
 		@meta_header = "История заказов"
 		#Выбираем все заказы которые создавал данный пользователь
 		@like_orders = LikeOrder.find(:all, :conditions => {:user_id => current_user.id })
 		@subscribers = SubscribeOrders.find(:all, :conditions => {:user_id => current_user.id })
 		@friends = FriendOrder.find(:all, :conditions => {:user_id => current_user.id })
	end

  def check_like4me_subscribe
          vk = VkontakteApi::Client.new(get_settings_value('access_token'))
          is_member = vk.groups.isMember(gid: '64941464', user_id: current_user.uid)
          if is_member.to_i == 1
            render :json => {error_code: 0} #Вы в группе все хорошо
            return
          else
            render :json => {error_code: 1} #Вы не в группе проекта
            return
          end 
  end
  
  #Список всех заявок в друзья
  def friends
    if !current_user
      redirect_to '/'
    end
    #Забиваем все параметры в хеш
    params = {:user_id => current_user.id, :user_sex => current_user.sex, :uid => current_user.uid.to_i, :time_expiration => Time.now.to_time.to_i}
    #Составляем условие по базовым критериям
    condition = "`user_id` <> :user_id AND uid <> :uid AND (`sex` = :user_sex OR `sex` = 0) AND (`time_expiration` >= :time_expiration) AND ((`gender_min` = 0 AND `gender_max` = 0)"
    #Если дата рождения текущего пользователя не nil
    if current_user.birthday!=nil 
      #Высчитываем возраст
      bdate = (((Date.today-Date.parse(current_user.birthday.to_s)).to_i)/365).to_i
      #Составляем условия на возраст
      condition = condition + " OR (`gender_min` >= "+bdate.to_s+" AND `gender_max` = 0)"
      condition = condition + " OR (`gender_min` = 0 AND `gender_max` <= "+bdate.to_s+")"
      condition = condition + " OR (`gender_min` >= "+bdate.to_s+" AND `gender_max` <= "+bdate.to_s+")"
    end
    condition = condition + ')'
    #Выбираем все заказы удовл. условию
    @friends = FriendOrder.find(:all, :conditions => [condition,  params], :order => 'bonus DESC', :limit => 12)
    @meta_title = "Все заявки на друзья - Личный кабинет"
  end
  
  #Форма на скачку подписчиков(друзей)
  def download_subscribers
    if !current_user
      redirect_to '/'
    end
    
    @meta_title = 'Личный кабинет'
    @meta_header = 'Список подписчиков (друзей)'
    #Созадем модель из поста
    @download_subscribers = DownloadSubscribers.new(params[:download_subscribers])
    #Если пришел пост
    if request.post? && !params[:download_subscribers].nil?
        #Если пост валидный
        if @download_subscribers.valid?
          #Шлум гет запрос на УРЛ 
          RestClient.get(params[:download_subscribers][:url]) { |response, request, result, &block|
            case response.code
            #Если поймали 404
            when 404
              #Шлем ошибку пользователю
                @download_subscribers.errors.add(:url, "который вы ввели недоступен")
            else
              #Шелм еще один гет запрос
              response = RestClient.get(params[:download_subscribers][:url])
              #Если пришла внутренняя ошибка
              if response.scan(/<title>Ошибка<\/title>/i).size > 0
                #Шлем ошибку пользователю
                @download_subscribers.errors.add(:url, "который вы ввели запрещён для общего доступа настройками приватности")
              else
                #Создаем объект для работы с ВК
                vk = VkontakteApi::Client.new(get_settings_value('access_token'))
                #Если тип аккаунт
                if @download_subscribers.type == 'account'
                  #Пытаемся получить Ид или имя 
                  uid = params[:download_subscribers][:url].split("/")[3]
                  #Начинаем транзакцию 
                  begin 
                    #Запрашиваем информацию об аккаунте
                    @user_info = vk.users.get(uids: uid, fields: 'photo_medium')
                  #Проверяем на наличиеошибки  
                  rescue VkontakteApi::Error => e
                    #Если поймали ошибку
                    if e.error_code == 113
                      #Шлем ее пользователю
                      @download_subscribers.errors.add(:url, "введен не верно. Мы не можем получить данные об аккаунте. Может это группа?") 
                      render :download_subscribers
                      return
                    end
                  end
                  #Запрашиваем список друзей
                  @friend_list = vk.friends.get(uid: @user_info[0].uid)
                  #Если количество друзей больше чем текущий баланс
                  if @friend_list.size > current_user.balance
                    #Шлем ошибку
                    @download_subscribers.errors.add(:url, "который вы ввели содержит слишком много друзей. Вашего баланса недостаточно.")  
                  end
                end
                #Если тип группа
                if @download_subscribers.type == 'group'
                  #Пытаемся получить уникальное имя или ид
                  gid = params[:download_subscribers][:url].split("/")[3]
                  #Начинаем транзакцию
                  begin
                    #Запращиваем информацию о группе
                    @group_info = vk.groups.getById(gid: gid)
                  #Если ловим ошибку  
                  rescue VkontakteApi::Error => e
                    if e.error_code == 100
                      #Шлем ее пользователю
                      @download_subscribers.errors.add(:url, "введен не верно. Мы не можем получить данные о группе. Может это аккаунт") 
                      render :download_subscribers
                      return
                    end
                  end
                  #Запрашиваем участников группы
                  @members = vk.groups.getMembers(gid: @group_info[0].gid, count: 1000)
                  #Если количество участников больше чем баланс
                  if @members[:count] > current_user.balance
                    #Шлем ошибку
                    @download_subscribers.errors.add(:url, "который вы ввели содержит слишком много подписчиков. Вашего баланса недостаточно.")  
                  end
                end
                
              end
            end
          }
        end
    end
  end
  
  #Создаем список для пользователя
  def download_list
    if !current_user
      redirect_to '/'
      return
    end 
    
    #Если нет парметра ида группы или пользователя уходим отсда
    if !params[:uid] && !params[:gid]
      redirect_to '/'
      return
    end
    
    #Создаем модель
    download_order = DownloadOrders.new
    #Если параметр принадлежит группе
    if params[:gid]
      #Создаем объект для работы с ВК
      vk = VkontakteApi::Client.new(get_settings_value('access_token'))
      #Сюда будем скидывать список
      @global_users_list = Array.new
      gid = params[:gid]
      #НАчинаем транзакцию
      begin
        #Запрашиваем информацию о группе
        @group_info = vk.groups.getById(gid: gid)
      #Если ловим ошибку    
      rescue VkontakteApi::Error => e
        #Отправляем на страницу 500
        if e.error_code == 100
          redirect_to '/500'
          return
        end
      end
      #Получаем список пользователей
      @members = vk.groups.getMembers(gid: @group_info[0].gid, count: 1000, fields: 'sex, bdate, city, country')
      #Если количество пользователей больше чем баланс
      if @members[:count] > current_user.balance
        #Отправляем на страницу 500
        redirect_to '/500'
        return 
      end
      #Определяем сколько шагов надо будет сделать
      step = @members[:count]/1000
      #Запускам цикл
      for i in 0..step.to_i
       #Для первого шага запрос уже был сделан
       if i != 0
         #Если шаг не первый запрашиваем количество со смещением
         @members = vk.groups.getMembers(gid: @group_info[0].gid, count: 1000, offset: i*1000, fields: 'sex, bdate, city, country')
       end
       #Скидываем подписчиков
       @global_users_list = @global_users_list+@members.users 
      end
      #Забиваем данные в модель заказа
      download_order.type = 'group'
      download_order.count_users = @global_users_list.size
      download_order.vkid = params[:gid]
      download_order.user_id = current_user.id
      #Сохраняем модеь
      download_order.save
      #Уменьшаем баланс пользователя
      site_user = SiteUser.find(:first, :conditions => {:id => current_user.id })
      site_user.update_attributes(:balance => current_user.balance.to_i - @global_users_list.size.to_i)
    end
    
    #Если пришел параметр друг
    if params[:uid]
      #Создем объект в вк
      vk = VkontakteApi::Client.new(get_settings_value('access_token'))
      #Сюда скидыаем пользователей
      @global_users_list = Array.new
      uid = params[:uid]   
      #Начало транзакции
      begin
        #Запрашиваем список пользователей
        @friend_list = vk.friends.get(uid: uid, fields: 'sex, bdate, city, country')
      #Если поймали ошибку  
      rescue VkontakteApi::Error => e
        if e.error_code
          #Отправляем на страницу 500
          redirect_to '/500'
          return
        end
      end 
      #Скидываем пользователей
      @global_users_list += @friend_list
      #Забиваем данные в модель 
      download_order.type = 'account'
      download_order.count_users = @global_users_list.size
      download_order.vkid = params[:uid]
      download_order.user_id = current_user.id
      download_order.save
      #Уменьшеаем баланс
      site_user = SiteUser.find(:first, :conditions => {:id => current_user.id })
      site_user.update_attributes(:balance => current_user.balance.to_i - @global_users_list.size.to_i)
    end
    #Отправляем заголовки
    headers['Content-Type'] = "application/vnd.ms-excel"
    headers['Content-Disposition'] = 'attachment; filename="uses.xls"'
    headers['Cache-Control'] = ''
    #Рендерим представление
    render :users_list, layout: false
  end
end