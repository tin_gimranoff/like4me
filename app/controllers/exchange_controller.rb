class ExchangeController < ApplicationController

  layout "admin"

  before_filter :authenticate_user!

  def index
    @exchange = Exchange.find(:all, :order => "`status`, `id` DESC" )

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  def show
    @exchange = Exchange.find(params[:id])
    @exchange.update_attribute(:status, 1)
    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @exchange = Exchange.find(params[:id])
    @exchange.destroy

    respond_to do |format|
      format.html { redirect_to '/admin/exchange' }
    end
  end
end
