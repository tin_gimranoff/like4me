# coding: utf-8
class Payment < ActiveRecord::Base
  attr_accessible :user_id, :amount, :status, :payment_num
  
  def create_url(spShopPaymentId, spAmount, spUserDataUserId)
    url = 'http://sprypay.ru/sppi/'
    url = url + '?spShopId=216377'
    url = url + '&spShopPaymentId='+spShopPaymentId
    url = url + '&spAmount='+spAmount
    url = url + '&spUserDataUserId='+spUserDataUserId
    url = url + '&spCurrency=rur'
    url = url + '&spPurpose=Покупка сердечек'
    url = url + '&lang=ru'
    return url
  end
  
end