class DownloadSubscribers < ActiveRecord::Base
  
  include AdminHelper
  
  attr_accessible :type, :url, :user_id
  
    validates :url, :presence => true,
            :format => { :with => /^http[s]{0,1}:\/\/vk\.com\/.*$/i }
            
    validates :type, :presence => true
    
    def get_city_by_id(id, country_id)
      access_token = get_settings_value('access_token')
      vk = VkontakteApi::Client.new(access_token)
      info = vk.places.getCityById(cids: id) 
      city = VkCities.new
      if country_id
        country = VkCountries.find(:first, :conditions =>{:vkid => country_id}) 
        city.city_id = country.id
        city.city_vkid = country.vkid
      end
      city.city = info[0].name
      city.vkid = info[0].cid
      city.save
      sleep 3
      return info[0].name
    end
    
    def get_country_by_id(id)
      access_token = get_settings_value('access_token')
      vk = VkontakteApi::Client.new(access_token)
      info = vk.places.getCountryById(cids: id)
      country = VkCountries.new
      country.vkid = info[0].cid
      country.country = info[0].name
      country.save
      sleep 3
      return info[0].name
    end
end
