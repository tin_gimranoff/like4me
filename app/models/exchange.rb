class Exchange < ActiveRecord::Base
  attr_accessible :balance, :user_id, :wmr, :status
  
  validates :wmr, :presence => true,
            :format => { :with => /^R[0-9]{12}$/i }
            
  validates :balance, :presence => true,
            :numericality => { :only_integer => true, :greater_than_or_equal_to => 100 }
end
