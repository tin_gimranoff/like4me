class FriendOrder < ActiveRecord::Base
  attr_accessible :balance, :bonus, :image, :name, :url, :user_id, :gender_min, :gender_max, :sex, :uid, :time_expiration
  
  validates :balance, :presence => true,
            :numericality => { :only_integer => true, :greater_than => 0 }
            
  validates :bonus, :presence => true,
            :numericality => { :only_integer => true, :greater_than => 0 }
            
  validates :url, :presence => true,
            :format => { :with => /^http[s]{0,1}:\/\/vk\.com\/.*$/i },
            :uniqueness => true
            
  def check_friend(access_token, uid)
    vk = VkontakteApi::Client.new(access_token)
    info = vk.friends.get(uid: uid)
    if self.uid.in?(info)
      return 1
    else
      return 0
    end
  end
  
  def calculate_time_expiration
    balance_in_seconds = self.balance.to_i*60.to_i
    self.time_expiration = Time.now.to_time.to_i+balance_in_seconds
  end
  
  def get_expiration_time
    exp_time = self.time_expiration.to_i - Time.now.to_time.to_i
    if exp_time >= 0 
      return (exp_time/60).to_i
    else
      return 0
    end
  end
end