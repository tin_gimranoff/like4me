class SubscribeOrders < ActiveRecord::Base
  attr_accessible :balance, :bonus, :gid, :name, :url, :user_id, :country, :sex, :gender_min, :gender_max
  
  validates :balance, :presence => true,
  					  :numericality => { :only_integer => true, :greater_than_or_equal_to => 10 }					  
  validates :bonus, :presence => true,
              :numericality => { :only_integer => true, :greater_than => 1 }
  validates :name, :presence => true,
              :uniqueness => true
  validates :url, :presence => true,
              :format => { :with => /^http[s]{0,1}:\/\/vk\.com\/.*$/i },
              :uniqueness => true
end
