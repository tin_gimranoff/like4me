module ExchangeHelper
  def get_count_unread_exchange_orders
    return Exchange.count(:all, :conditions => { :status => 0 })
  end
end
